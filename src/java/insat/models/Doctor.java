/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Madou
 */
@Entity
@Table(name = "DOCTOR")
public class Doctor implements Serializable {

    @Id
    @Column(name = "DOCTOR_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private long doctor_ID;

    @Column(name = "DOCTOR_FIRSTNAME")
    private String doctor_Firstname;

    @Column(name = "DOCTOR_LASTNAME")
    private String doctor_Lastname;

    @Column(name = "DOCTOR_EMAIL")
    private String doctor_Email;

    @Column(name = "DOCTOR_BIRTHDATE")
    private String doctor_Birthdate;

    @Column(name = "DOCTOR_PICTURE")
    private String doctor_Picture;

    @Column(name = "DOCTOR_INTRO")
    private String doctor_Intro;

    @Column(name = "DOCTOR_NUMBER")
    private long doctor_Number;

    @Column(name = "DOCTOR_PASSWORD")
    private String doctor_Password;

    @Column(name = "DOCTOR_SKILLS")
    private String doctor_skills;

    @Column(name = "DOCTOR_CONTACT")
    private String doctor_contact;

    @Column(name = "DOCTOR_HOBBIES")
    private String doctor_hobbies;

    public Doctor() {
    }

    public Doctor(long doctor_ID, String doctor_Firstname, String doctor_Lastname, String doctor_Email, String doctor_Birthdate, String doctor_Picture, String doctor_Intro, long doctor_Number, String doctor_Password, String doctor_skills, String doctor_contact, String doctor_hobbies) {
        this.doctor_ID = doctor_ID;
        this.doctor_Firstname = doctor_Firstname;
        this.doctor_Lastname = doctor_Lastname;
        this.doctor_Email = doctor_Email;
        this.doctor_Birthdate = doctor_Birthdate;
        this.doctor_Picture = doctor_Picture;
        this.doctor_Intro = doctor_Intro;
        this.doctor_Number = doctor_Number;
        this.doctor_Password = doctor_Password;
        this.doctor_skills = doctor_skills;
        this.doctor_contact = doctor_contact;
        this.doctor_hobbies = doctor_hobbies;
    }

    public long getDoctor_ID() {
        return doctor_ID;
    }

    public void setDoctor_ID(long doctor_ID) {
        this.doctor_ID = doctor_ID;
    }

    public String getDoctor_Firstname() {
        return doctor_Firstname;
    }

    public void setDoctor_Firstname(String doctor_Firstname) {
        this.doctor_Firstname = doctor_Firstname;
    }

    public String getDoctor_Lastname() {
        return doctor_Lastname;
    }

    public void setDoctor_Lastname(String doctor_Lastname) {
        this.doctor_Lastname = doctor_Lastname;
    }

    public String getDoctor_Email() {
        return doctor_Email;
    }

    public void setDoctor_Email(String doctor_Email) {
        this.doctor_Email = doctor_Email;
    }

    public String getDoctor_Birthdate() {
        return doctor_Birthdate;
    }

    public void setDoctor_Birthdate(String doctor_Birthdate) {
        this.doctor_Birthdate = doctor_Birthdate;
    }

    public String getDoctor_Picture() {
        return doctor_Picture;
    }

    public void setDoctor_Picture(String doctor_Picture) {
        this.doctor_Picture = doctor_Picture;
    }

    public String getDoctor_Intro() {
        return doctor_Intro;
    }

    public void setDoctor_Intro(String doctor_Intro) {
        this.doctor_Intro = doctor_Intro;
    }

    public long getDoctor_Number() {
        return doctor_Number;
    }

    public void setDoctor_Number(long doctor_Number) {
        this.doctor_Number = doctor_Number;
    }

    public String getDoctor_Password() {
        return doctor_Password;
    }

    public void setDoctor_Password(String doctor_Password) {
        this.doctor_Password = doctor_Password;
    }

    public String getDoctor_skills() {
        return doctor_skills;
    }

    public void setDoctor_skills(String doctor_skills) {
        this.doctor_skills = doctor_skills;
    }

    public String getDoctor_contact() {
        return doctor_contact;
    }

    public void setDoctor_contact(String doctor_contact) {
        this.doctor_contact = doctor_contact;
    }

    public String getDoctor_hobbies() {
        return doctor_hobbies;
    }

    public void setDoctor_hobbies(String doctor_hobbies) {
        this.doctor_hobbies = doctor_hobbies;
    }

}
