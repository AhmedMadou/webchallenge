/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

/**
 *
 * @author Mohamed
 */
import insat.models.Login;
import insat.models.Users;
import java.util.List;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.*;
import org.hibernate.service.ServiceRegistry;

public class UserDaoImpl implements UserDao {

    public void register(Users user) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();

        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            session.save(user);
            //commit the transaction
            session.getTransaction().commit();
        } finally {
            session.close();
        }

        System.out.println("Registering user with name : " + user.getUser_Firstname());
    }

    public Users validateUser(Login login) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Users> user;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User as u where u.user_Email='" + login.getEmail() + "' and u.user_Password='" + login.getPassword() + "'";
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (!user.isEmpty()) {
            return user.get(0);
        }
        return null;

    }

    public List<Users> userSearch(String name) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Users> user;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User as u where lower(u.user_Firstname) like lower('%" + name + "%') or lower(u.user_Lastname) like lower('%" + name + "%') or u.user_Email='" + name + "' or str(u.user_Number)='" + name + "' or concat(u.user_Firstname, ' ', u.user_Lastname)='" + name + "' or concat(u.user_Lastname, ' ', u.user_Firstname)='" + name + "'";
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        return user;
    }

    public Users getUser(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        Users user = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            /*String query = "from User as u where u.user_ID=" + id;
            System.out.println("query is : " + query);
            user = session.createQuery(query).list();*/

            user = (Users) session.get(Users.class, id);
            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

        if (user != null) {
            return user;
        }
        return null;

    }

    @Override
    public List<Users> getUsers() {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        List<Users> users = null;
        try {

            //start the transaction
            session.beginTransaction();
            //save the user object
            String query = "from User";
            System.out.println("query is : " + query);
            users = session.createQuery(query).list();

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }
        return users;
    }

    @Override
    public void deleteUser(long id) {

        // creating session factory
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry;
        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Users.class)
                .buildSessionFactory(serviceRegistry);

        //creating session
        Session session = factory.openSession();
        Users user = null;
        try {

            //start the transaction
            session.beginTransaction();
            //get the user object
            user = (Users) session.get(Users.class, id);

            //delete the user
            session.delete(user);

            //commit the transaction
            session.getTransaction().commit();

        } finally {
            session.close();
        }

    }

}
