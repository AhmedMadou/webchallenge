/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.dao;

/**
 *
 * @author Mohamed
 */
import insat.models.Login;
import insat.models.Users;
import java.util.List;

public interface UserDao {

    public void register(Users user);

    public Users validateUser(Login login);

    public List<Users> userSearch(String name);

    public Users getUser(long id);

    public List<Users> getUsers();

    public void deleteUser(long id);

}
