/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Login;
import insat.models.Users;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/*
 *
 * @author Mohamed
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session_SocialMedia = request.getSession(false);
        if (null == session_SocialMedia.getAttribute("Session_user")) {
            ModelAndView mav = new ModelAndView("redirect:/login");
            mav.addObject("login", new Login());
            return mav;
        }
        return new ModelAndView("redirect:/welcome");
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView getConf(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("welcome");

        HttpSession session_SocialMedia = request.getSession(false);
        if (null == session_SocialMedia.getAttribute("Session_user")) {
            return new ModelAndView("redirect:/login");
        }

        Users user = new Users();
        user = (Users) session_SocialMedia.getAttribute("Session_user");
        mav.addObject("user", user);

        return mav;

    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session_SocialMedia = request.getSession(false);
        session_SocialMedia.invalidate();

        return new ModelAndView("redirect:/login");
    }

}
