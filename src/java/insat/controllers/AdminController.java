/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package insat.controllers;

import insat.models.Admin;
import insat.models.Login;
import insat.models.Users;
import insat.service.AdminService;
import insat.service.UserService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Madou
 */
@Controller
public class AdminController {

    @Autowired
    AdminService adminService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/MAM-admin", method = RequestMethod.GET)
    public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("adminLogIn");
        mav.addObject("login", new Login());
        return mav;
    }

    @RequestMapping(value = "/adminLoginProcess", method = RequestMethod.POST)
    public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,
            @ModelAttribute("login") Login login) {

        ModelAndView mav = null;

        System.out.println(login.getEmail() + "  " + login.getPassword());
        Admin admin = adminService.validateAdmin(login);

        if (null != admin) {
            mav = new ModelAndView("redirect:/adminWelcome");

            HttpSession session_SocialMedia = request.getSession();
            if (null == session_SocialMedia.getAttribute("Session_Admin")) {
                session_SocialMedia.setAttribute("Session_Admin", admin);
            }

        } else {
            mav = new ModelAndView("adminLogIn");
            mav.addObject("message", "Username or Password is wrong !");
        }
        return mav;
    }

}
